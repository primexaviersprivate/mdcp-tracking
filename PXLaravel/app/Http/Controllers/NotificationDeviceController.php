<?php

namespace App\Http\Controllers;

use App\Model\NotificationDevice;
use Illuminate\Http\Request;

class NotificationDeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\NotificationDevice  $notificationDevice
     * @return \Illuminate\Http\Response
     */
    public function show(NotificationDevice $notificationDevice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\NotificationDevice  $notificationDevice
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificationDevice $notificationDevice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\NotificationDevice  $notificationDevice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NotificationDevice $notificationDevice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\NotificationDevice  $notificationDevice
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotificationDevice $notificationDevice)
    {
        //
    }
}
