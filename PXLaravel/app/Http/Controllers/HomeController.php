<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\ChartSuhu;
use App\SensorData;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sensorDataId = SensorData::get()->pluck('id');
        $sensorDataData = SensorData::get()->pluck('data');
        $chart = new ChartSuhu;
        $chart->labels($sensorDataId);
        $chart->dataset('Sensor Data', 'line', $sensorDataData);
        return view('home', ['chart' => $chart]);
    }
}
