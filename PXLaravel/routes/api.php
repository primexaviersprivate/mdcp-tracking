<?php

use Illuminate\Http\Request;
use App\SensorData;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/sendNotification' , 'NotificationController@SendNotification');

Route::post('/getTherm' , function (Request $request) {
    if($request->sensordata != null){
        $addDataSensor = new SensorData;
        $addDataSensor->sensor_id = 1;
        $addDataSensor->data = $request->sensordata;
        $addDataSensor->save();
        if($addDataSensor){
            if($request->sensorData > 30){                
                $data = array('name'=>"System");
                Mail::send('mailtemplate\mail', $data, function($message) {
                    $message->to('mraldiafandi@gmail.com', 'Notifier')->subject('Alert');
                    $message->from(env("MAIL_FROM_ADDRESS","mdcp@primexaviers.com"),env("MAIL_FROM_NAME","MDCP System"));
                });
            }
            return "success";
        }else{
            return "add failed";
        }
    }else{
        return "sorry, Failed To Add";  
    }
});