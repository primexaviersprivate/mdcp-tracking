@extends('layouts.app')
@section('headerScript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <button onclick="loadDoc()">
                            Click here To send Notification
                        </button>
                        <div id="demo">
                            {!! $chart->container() !!}
                        </div>
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customScript')
<script src="https://unpkg.com/vue"></script>
        <script>
            var app = new Vue({
                el: '#demo',
            });
        </script>
        <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>
        {!! $chart->script() !!}
<script>
function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("demo").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/api/sendNotification", true);
  xhttp.send();
}
</script>
@endsection
